import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/home/Home.vue'
import AudioDemo from './views/Audio.vue'
import Podcast from './views/Podcast.vue'

import VideoDemo from './views/Video.vue'
import Playground from './views/Playground.vue'
import Docs from './views/Docs.vue'
import Support from './views/Support.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/audio',
      name: 'audiodemo',
      component: AudioDemo
    },
    {
      path: '/podcast',
      name: 'podcast',
      component: Podcast
    },
    {
      path: '/video',
      name: 'videodemo',
      component: VideoDemo
    },
    {
      path: '/playground',
      name: 'playground',
      component: Playground
    },
    {
      path: '/docs',
      name: 'docs',
      component: Docs
    },
    {
      path: '/support',
      name: 'support',
      component: Support
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
