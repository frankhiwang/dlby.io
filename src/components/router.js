import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/home/Home.vue'
import GetStarted from './views/GetStarted.vue'
import Docs from './views/Docs.vue'
import Support from './views/Support.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/getstarted',
      name: 'getstarted',
      component: GetStarted
    },
    {
      path: '/docs',
      name: 'docs',
      component: Docs
    },
    {
      path: '/support',
      name: 'support',
      component: Support
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
